This is an example Repository
=============================

You can use [Markdown](https://confluence.atlassian.com/display/STASH038/Markdown+syntax+guide) 
to make a nice readme file for other users explaining the content of the
repository which includes

1. ordered lists
2. unordered lists, for example
   - one item
   - second item
3. links, either just by writing http://myurl.com or [Linktext](http://myurl.com)
4. tables, see below

and so forth. Markdown is designed to look as readable as possible also in text
editors and for basic things it's very easy to use:

| header1    | header2    |
|------------|------------|
| row1, col1 | row1, col2 |
| row2, col1 | row2, col2 |


Add a subsection
----------------

You can easily add subsections to the readme

Merge Conflicts
---------------

Merge conflicts happen if more then one commit try to edit the same part of the
same file. Git is rather good in trying to avoid them but it cannot do this all
the time.

MASTER BRANCH
